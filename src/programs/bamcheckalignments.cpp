/**
    bambam
    Copyright (C) 2009-2013 German Tischler
    Copyright (C) 2011-2013 Genome Research Limited

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/
#include <config.h>
#include <iostream>
#include <cstdlib>
#include <libmaus2/util/ArgInfo.hpp>
#include <libmaus2/util/OutputFileNameTools.hpp>
#include <libmaus2/util/GetFileSize.hpp>
#include <libmaus2/fastx/FastAReader.hpp>
#include <libmaus2/util/TempFileRemovalContainer.hpp>
#include <libmaus2/bambam/BamDecoder.hpp>
#include <libmaus2/bambam/BamWriter.hpp>
#include <libmaus2/bambam/BamHeaderUpdate.hpp>
#include <libmaus2/bambam/BamBlockWriterBaseFactory.hpp>
#include <libmaus2/bambam/BamMultiAlignmentDecoderFactory.hpp>
#include <libmaus2/lcs/NPLinMem.hpp>

#include <libmaus2/lz/BgzfDeflateOutputCallbackMD5.hpp>
#include <libmaus2/bambam/BgzfDeflateOutputCallbackBamIndex.hpp>
static int getDefaultMD5() { return 0; }
static int getDefaultIndex() { return 0; }

std::string toUpper(std::string s)
{
	for ( uint64_t i = 0; i < s.size(); ++i )
		s[i] = toupper(s[i]);

	return s;
}

bool checkCigarValid(
	::libmaus2::bambam::BamAlignment const & alignment,
	::libmaus2::bambam::BamHeader const & bamheader,
	::libmaus2::autoarray::AutoArray < ::libmaus2::autoarray::AutoArray<uint8_t>::unique_ptr_type > const & text
)
{
	if ( alignment.isUnmap() )
		return true;

	if ( ! alignment.isCigarLengthConsistent() )
	{
		std::cerr << "[E] inconsistent cigar " << alignment.getCigarString() << " for " << alignment.getName()
			<< " " << alignment.getLseqByCigar() << " != " << alignment.getLseq() << std::endl;
		return false;
	}

	if ( alignment.getRefID() < 0 || alignment.getRefID() >= static_cast<int64_t>(bamheader.getNumRef()) )
	{
		std::cerr << "[E] reference id " << alignment.getRefID() << " out of range for " << alignment.getName() << std::endl;
		return false;
	}

	::libmaus2::autoarray::AutoArray<uint8_t> const & ctext = *(text[alignment.getRefID()]);
	int64_t refpos = alignment.getPos();
	int64_t seqpos = 0;
	bool alok = true;
	std::string const read = alignment.getRead();

	for ( uint64_t i = 0; alok && i < alignment.getNCigar(); ++i )
	{
		char const cop = alignment.getCigarFieldOpAsChar(i);
		int64_t const clen = alignment.getCigarFieldLength(i);

		switch ( cop )
		{
			// match/mismatch, increment both
			case '=':
			case 'X':
			case 'M':
			{
				for ( int64_t j = 0; alok && j < clen; ++j, ++refpos, ++ seqpos )
				{
					if ( refpos < 0 || refpos >= static_cast<int64_t>(ctext.size()) )
					{
						std::cerr << "[E] " << cop << " operation outside of chromosome coordinate range " << " for " << alignment.getName()
							<< " refpos=" << refpos
							<< " seqpos=" << seqpos
							<< std::endl;
						alok = false;
					}
					else if ( seqpos >= alignment.getLseq() )
					{
						std::cerr << "[E] " << cop << " operation outside of sequence coordinate range " << " for " << alignment.getName()
							<< " refpos=" << refpos
							<< " seqpos=" << seqpos
							<< std::endl;
						alok = false;
					}
					else if ( cop == '=' && toupper(ctext[refpos]) != toupper(read[seqpos]) )
					{
						std::cerr << "[E] " << cop << " operation but mismatch between reference and query for " << alignment.getName()
							<< " refpos=" << refpos
							<< " seqpos=" << seqpos
							<< std::endl;
						alok = false;
					}
					else if ( cop == 'X' && toupper(ctext[refpos]) == toupper(read[seqpos]) )
					{
						std::cerr << "[E] " << cop << " operation but match between reference and query for" << alignment.getName()
							<< " refpos=" << refpos
							<< " seqpos=" << seqpos
							<< std::endl;
						alok = false;
					}
				}
				break;
			}
			// insert into reference, increment seq
			case 'P':
			case 'I':
			{
				for ( int64_t j = 0; alok && j < clen; ++j, ++seqpos )
				{
					if ( seqpos >= alignment.getLseq() )
					{
						std::cerr << "[E] " << cop << " operation outside of sequence coordinate range " << " for " << alignment.getName() << std::endl;
						alok = false;
					}
				}
				break;
			}
			// delete from reference, increment ref
			case 'D':
			{
				for ( int64_t j = 0; alok && j < clen; ++j, ++refpos )
				{
					if ( refpos < 0 || refpos >= static_cast<int64_t>(ctext.size()) )
					{
						std::cerr << "[E] " << cop << " operation outside of reference coordinate range " << " for " << alignment.getName() << std::endl;
						alok = false;
					}
				}
				break;
			}
			// soft clipping, increment seq
			case 'S':
			{
				for ( int64_t j = 0; alok && j < clen; ++j, ++seqpos )
				{
					if ( seqpos >= alignment.getLseq() )
					{
						std::cerr << "[E] " << cop << " operation outside of sequence coordinate range " << " for " << alignment.getName() << std::endl;
						alok = false;
					}
				}
				break;
			}
			// hard clipping, do nothing
			case 'H':
			{
				break;
			}
			// skip region in reference, increment ref
			case 'N':
			{
				for ( int64_t j = 0; alok && j < clen; ++j, ++refpos )
				{
					if ( refpos < 0 || refpos >= static_cast<int64_t>(ctext.size()) )
					{
						std::cerr << "[E] " << cop << " operation outside of reference coordinate range " << " for " << alignment.getName() << std::endl;
						alok = false;
					}
				}
				break;
			}
		}
	}

	if ( ! alok )
	{
		std::cerr << "[E] broken alignment " << alignment.formatAlignment(bamheader) << std::endl;

		uint64_t const fsc = alignment.getFrontSoftClipping();
		uint64_t const rsc = alignment.getBackSoftClipping();

		std::string const read = toUpper(alignment.getRead().substr(0,alignment.getLseq()-rsc).substr(fsc));
		std::string const ref = toUpper(std::string(
			ctext.begin() + alignment.getPos(),
			ctext.begin() + alignment.getPos() + alignment.getReferenceLength()
		));

		std::cerr << "read=" << read << std::endl;
		std::cerr << "ref =" << ref << std::endl;

		libmaus2::lcs::NPLinMem np;
		np.np(ref.begin(),ref.end(),read.begin(),read.end());

		libmaus2::autoarray::AutoArray< std::pair<libmaus2::lcs::AlignmentTraceContainer::step_type,uint64_t> > Aopblocks;
		libmaus2::autoarray::AutoArray<libmaus2::bambam::cigar_operation> Aop;

		uint64_t const ncig = libmaus2::bambam::CigarStringParser::traceToCigar(np,Aopblocks,Aop,0,fsc,rsc,0);

		std::cerr << np.getAlignmentStatistics() << std::endl;

		std::ostringstream cigstr;
		libmaus2::bambam::CigarStringParser::cigarBlocksToString(cigstr,Aop.begin(),ncig);

		std::cerr << "updated cigar " << cigstr.str() << std::endl;
	}

	return alok;
}

int main(int argc, char * argv[])
{
	try
	{
		::libmaus2::util::ArgInfo const arginfo(argc,argv);
		::libmaus2::util::TempFileRemovalContainer::setup();

		::std::vector<std::string> const & inputfilenames = arginfo.restargs;
		char const * fasuffixes[] = { ".fa", ".fasta", 0 };
		std::string defoutname = libmaus2::util::OutputFileNameTools::endClipLcp(inputfilenames,&fasuffixes[0]) + ".fa";
		while ( ::libmaus2::util::GetFileSize::fileExists(defoutname) )
			defoutname += "_";
		std::string const fatempfilename = arginfo.getValue<std::string>("fatempfilename",defoutname);
		::libmaus2::util::TempFileRemovalContainer::addTempFile(fatempfilename);
		int const replacequery = arginfo.getValue<int>("replacequery",1);
		int const replacecigar = arginfo.getValue<int>("replacecigar",1);

		// std::cerr << "output file name " << defoutname << std::endl;

		::std::vector< ::libmaus2::fastx::FastAReader::RewriteInfo > const info = ::libmaus2::fastx::FastAReader::rewriteFiles(inputfilenames,fatempfilename);

		std::map < std::string, uint64_t > fachr;
		::libmaus2::autoarray::AutoArray < uint64_t > fapref(info.size()+1);
		for ( uint64_t i = 0; i < info.size(); ++i )
		{
			// std::cerr << info[i].valid << "\t" << info[i].idlen << "\t" << info[i].seqlen << "\t" << info[i].getIdPrefix() << std::endl;
			fachr[info[i].getIdPrefix()] = i;
			fapref [ i ] = info[i].getEntryLength() ;
		}
		fapref.prefixSums();
		for ( uint64_t i = 0; i < info.size(); ++i )
			fapref [ i ] += info[i].idlen + 2; // > + newline

		// input decoder wrapper
		libmaus2::bambam::BamAlignmentDecoderWrapper::unique_ptr_type decwrapper(
			libmaus2::bambam::BamMultiAlignmentDecoderFactory::construct(
				arginfo,false, // do not put rank
				0, /* copy stream */
				std::cin, /* standard input */
				true, /* concatenate instead of merging */
				false /* streaming */
			)
		);
		::libmaus2::bambam::BamAlignmentDecoder * ppdec = &(decwrapper->getDecoder());
		::libmaus2::bambam::BamAlignmentDecoder & dec = *ppdec;
		::libmaus2::bambam::BamHeader const & bamheader = dec.getHeader();

		::libmaus2::autoarray::AutoArray<uint8_t> uptab(256,false);
		for ( uint64_t j = 0; j < uptab.size(); ++j )
			uptab[j] = toupper(j);

		::libmaus2::autoarray::AutoArray < ::libmaus2::autoarray::AutoArray<uint8_t>::unique_ptr_type > text(bamheader.getNumRef());
		for ( uint64_t i = 0; i < bamheader.getNumRef(); ++i )
		{
			std::string const bamchrname = bamheader.getRefIDName(i);
			if ( fachr.find(bamchrname) == fachr.end() )
			{
				::libmaus2::exception::LibMausException se;
				se.getStream() << "Unable to find reference sequence " << bamchrname << " in fa file." << std::endl;
				se.finish();
				throw se;
			}
			uint64_t const faid = fachr.find(bamchrname)->second;
			if ( bamheader.getRefIDLength(i) != static_cast<int64_t>(info[faid].seqlen) )
			{
				::libmaus2::exception::LibMausException se;
				se.getStream() << "Reference sequence " << bamchrname << " has len " << bamheader.getRefIDLength(i) << " in bam file but " << info[faid].seqlen << " in fa file." << std::endl;
				se.finish();
				throw se;
			}

			if ( bamheader.getNumRef() < 100 )
				std::cerr << "Loading sequence " << bamchrname << " of length " << info[faid].seqlen << std::endl;
			::libmaus2::autoarray::AutoArray<uint8_t>::unique_ptr_type ttext(new ::libmaus2::autoarray::AutoArray<uint8_t>(info[faid].seqlen,false));
			text [ i ] = UNIQUE_PTR_MOVE(ttext);
			::libmaus2::aio::InputStreamInstance CIS(fatempfilename);
			CIS.seekg(fapref[faid]);
			CIS.read(reinterpret_cast<char *>(text[i]->begin()),info[faid].seqlen);
			// sanity check, next symbol in file should be a newline
			int c;
			c = CIS.get();
			assert ( c == '\n' );

			// convert to upper case
			for ( uint8_t * pa = text[i]->begin(); pa != text[i]->end(); ++pa )
				*pa = uptab[*pa];
		}

		for ( uint64_t i = 0; i < bamheader.getNumRef(); ++i )
		{
			assert ( static_cast<int64_t>(text[i]->size()) == bamheader.getRefIDLength(i) );
		}

		uint64_t decoded = 0;

		/*
		 * start index/md5 callbacks
		 */
		std::string const tmpfilenamebase = arginfo.getValue<std::string>("tmpfile",arginfo.getDefaultTmpFileName());
		std::string const tmpfileindex = tmpfilenamebase + "_index";
		::libmaus2::util::TempFileRemovalContainer::addTempFile(tmpfileindex);

		std::string md5filename;
		std::string indexfilename;

		std::vector< ::libmaus2::lz::BgzfDeflateOutputCallback * > cbs;
		::libmaus2::lz::BgzfDeflateOutputCallbackMD5::unique_ptr_type Pmd5cb;
		if ( arginfo.getValue<unsigned int>("md5",getDefaultMD5()) )
		{
			if ( libmaus2::bambam::BamBlockWriterBaseFactory::getMD5FileName(arginfo) != std::string() )
				md5filename = libmaus2::bambam::BamBlockWriterBaseFactory::getMD5FileName(arginfo);
			else
				std::cerr << "[V] no filename for md5 given, not creating hash" << std::endl;

			if ( md5filename.size() )
			{
				::libmaus2::lz::BgzfDeflateOutputCallbackMD5::unique_ptr_type Tmd5cb(new ::libmaus2::lz::BgzfDeflateOutputCallbackMD5);
				Pmd5cb = UNIQUE_PTR_MOVE(Tmd5cb);
				cbs.push_back(Pmd5cb.get());
			}
		}
		libmaus2::bambam::BgzfDeflateOutputCallbackBamIndex::unique_ptr_type Pindex;
		if ( arginfo.getValue<unsigned int>("index",getDefaultIndex()) )
		{
			if ( libmaus2::bambam::BamBlockWriterBaseFactory::getIndexFileName(arginfo) != std::string() )
				indexfilename = libmaus2::bambam::BamBlockWriterBaseFactory::getIndexFileName(arginfo);
			else
				std::cerr << "[V] no filename for index given, not creating index" << std::endl;

			if ( indexfilename.size() )
			{
				libmaus2::bambam::BgzfDeflateOutputCallbackBamIndex::unique_ptr_type Tindex(new libmaus2::bambam::BgzfDeflateOutputCallbackBamIndex(tmpfileindex));
				Pindex = UNIQUE_PTR_MOVE(Tindex);
				cbs.push_back(Pindex.get());
			}
		}
		std::vector< ::libmaus2::lz::BgzfDeflateOutputCallback * > * Pcbs = 0;
		if ( cbs.size() )
			Pcbs = &cbs;
		/*
		 * end md5/index callbacks
		 */

		::libmaus2::bambam::BamHeader::unique_ptr_type uphead(libmaus2::bambam::BamHeaderUpdate::updateHeader(arginfo,bamheader,"bamcheckalignments",std::string(PACKAGE_VERSION)));
		//::libmaus2::bambam::BamWriter BW(std::cout,*uphead,Z_DEFAULT_COMPRESSION,Pcbs);
		libmaus2::bambam::BamBlockWriterBase::unique_ptr_type Uout ( libmaus2::bambam::BamBlockWriterBaseFactory::construct(*uphead, arginfo, Pcbs) );
		libmaus2::bambam::BamBlockWriterBase * Pout = Uout.get();

		while ( dec.readAlignment() )
		{
			++decoded;

			if ( decoded % (1024*1024) == 0 )
			{
				std::cerr << "[V] " << decoded << std::endl;
			}

			::libmaus2::bambam::BamAlignment & alignment = dec.getAlignment();

			bool const cigok = checkCigarValid(alignment,bamheader,text);

			// if cigar is ok then keep alignment
			if ( cigok )
			{
				if ( !alignment.isUnmap() )
				{
					uint64_t seqpos = 0;
					uint64_t refpos = alignment.getPos();
					std::string const read = alignment.getRead();
					std::string modseq = read;
					::libmaus2::autoarray::AutoArray<uint8_t> const & ctext = *(text[alignment.getRefID()]);

					std::ostringstream newcigarstream;

					for ( uint64_t i = 0; i < alignment.getNCigar(); ++i )
					{
						char const cop = alignment.getCigarFieldOpAsChar(i);
						int64_t const clen = alignment.getCigarFieldLength(i);

						switch ( cop )
						{
							// match/mismatch, increment both
							case 'M':
							{
								int64_t low = 0;

								while ( low != clen )
								{
									int64_t high = low;

									while ( high != clen && ctext[refpos] == read[seqpos] )
									{
										modseq[seqpos] = '=';
										++refpos, ++seqpos, ++ high;
									}
									if ( high != low )
										newcigarstream << high-low << "=";

									low = high;

									while ( high != clen && ctext[refpos] != read[seqpos] )
										++refpos, ++seqpos, ++ high;
									if ( high != low )
										newcigarstream << high-low << "X";

									low = high;
								}

								break;
							}
							case '=':
							{
								refpos += clen;
								for ( int64_t j = 0; j < clen; ++j, ++seqpos )
									modseq[seqpos] = '=';
								newcigarstream << clen << cop;
								break;
							}
							case 'X':
							{
								refpos += clen;
								seqpos += clen;
								newcigarstream << clen << cop;
								break;
							}
							case 'P':
							case 'I':
							{
								seqpos += clen;
								newcigarstream << clen << cop;
								break;
							}
							case 'N':
							case 'D':
							{
								refpos += clen;
								newcigarstream << clen << cop;
								break;
							}
							case 'S':
							{
								seqpos += clen;
								newcigarstream << clen << cop;
								break;
							}
							case 'H':
							{
								newcigarstream << clen << cop;
								break;
							}
						}
					}

					if ( replacecigar )
						alignment.replaceCigarString(newcigarstream.str());
					if ( replacequery )
						alignment.replaceSequence(modseq,alignment.getQual());
				}

				Pout->writeAlignment(alignment);
				// alignment.serialise(BW.getStream());
			}
		}

		if ( Pmd5cb )
		{
			Pmd5cb->saveDigestAsFile(md5filename);
		}
		if ( Pindex )
		{
			Pindex->flush(std::string(indexfilename));
		}

		Uout.reset();
	}
	catch(std::exception const & ex)
	{
		std::cerr << ex.what() << std::endl;
		return EXIT_FAILURE;
	}
}
